SET DATEFORMAT dmy
BULK INSERT a1700651.a1700651.[Entregan]
    FROM 'e:\wwwroot\a1700651\entregan.csv'
    WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )