BULK INSERT a1700651.a1700651.[Materiales]
   FROM 'e:\wwwroot\a1700651\materiales.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
