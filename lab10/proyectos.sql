BULK INSERT a1700651.a1700651.[Proyectos]
    FROM 'e:\wwwroot\a1700651\proyectos.csv'
    WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
