BULK INSERT a1700651.a1700651.[Proveedores]
    FROM 'e:\wwwroot\a1700651\proveedores.csv'
    WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
