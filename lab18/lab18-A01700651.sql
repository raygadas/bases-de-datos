-- La suma de las cantidades e importe total de todas las entregas realizadas durante el 97.
SELECT m.descripcion as Material, SUM(e.cantidad) as 'Cantidad entregada',
  SUM(e.cantidad*m.costo)*SUM(e.cantidad*m.costo*(1+m.PorcentajeImpuesto/100)) as 'Importe total'
FROM Materiales m, Entregan e
WHERE e.Clave = m.clave
GROUP BY m.Descripcion

-- Para cada proveedor, obtener la razón social del proveedor, número de entregas e importe total de las entregas realizadas.
SELECT pv.RazonSocial, count(pv.RazonSocial) as 'Numero de Entregas',
  SUM(e.cantidad*m.costo)*SUM(e.cantidad*m.costo*(1+m.PorcentajeImpuesto/100)) as 'Importe total'
FROM Proveedores pv, Entregan e, Materiales m
WHERE pv.RFC = e.RFC
AND m.Clave = e.Clave
GROUP BY pv.RazonSocial

-- Por cada material obtener la clave y descripción del material, la cantidad total entregada, la mínima cantidad entregada,
-- la máxima cantidad entregada, el importe total de las entregas de aquellos materiales en los que la cantidad promedio
-- entregada sea mayor a 400.
SELECT m.Clave, m.Descripcion, sum(e.Cantidad) as CantidadTotal, min(e.Cantidad) as CantidadMin, max(e.cantidad) as CantidadMax,
  SUM(e.cantidad*m.costo*(1+m.PorcentajeImpuesto/100)) as ImporteTotal
FROM Materiales m, Entregan e
WHERE m.Clave = e.Clave
GROUP BY m.clave, m.Descripcion
HAVING avg(e.cantidad) > 400

-- Para cada proveedor, indicar su razón social y mostrar la cantidad promedio de cada material entregado, detallando la clave
--  y descripción del material, excluyendo aquellos proveedores para los que la cantidad promedio sea menor a 500.
SELECT pv.RazonSocial, m.Descripcion, m.Clave, AVG(e.cantidad) as "Cantidad promedio entregada"
FROM Proveedores pv, Entregan e, Materiales m
WHERE pv.RFC = e.RFC
AND m.Clave = e.Clave
GROUP BY pv.RazonSocial, m.Descripcion, m.Clave
HAVING AVG(e.Cantidad) >= 500

-- Mostrar en una solo consulta los mismos datos que en la consulta anterior pero para dos grupos de proveedores: aquellos para
-- los que la cantidad promedio entregada es menor a 370 y aquellos para los que la cantidad promedio entregada sea mayor a 450.
SELECT pv.RazonSocial, m.Descripcion, m.Clave, AVG(e.cantidad) as 'Cantidad promedio entregada'
FROM Proveedores pv, Entregan e, Materiales m
WHERE pv.RFC = e.RFC
AND m.Clave = e.Clave
GROUP BY pv.RazonSocial, m.Descripcion, m.Clave
HAVING AVG(e.Cantidad) < 370 OR  AVG(e.Cantidad) > 450

-- INSERT INTO tabla VALUES (valorcolumna1, valorcolumna2, [...] , valorcolumnan)
INSERT INTO Materiales VALUES (1440, 'Tubos PVC', 150.00, 2.85)
INSERT INTO Materiales VALUES (1450, 'Laminas de acero', 300.00, 3.00)
INSERT INTO Materiales VALUES (1460, 'Tablas de madera', 250.00, 2.20)
INSERT INTO Materiales VALUES (1470, 'Impermiabilizante', 275.00, 2.80)
INSERT INTO Materiales VALUES (1480, 'Marmol', 290.00, 2.76)

-- Clave y descripción de los materiales que nunca han sido entregados.
SELECT m.Clave, m.Descripcion
FROM Materiales m
WHERE m.Clave NOT IN (
  SELECT m.Clave
  FROM Materiales m, Entregan e
  WHERE m.Clave = e.Clave
)

-- Razón social de los proveedores que han realizado entregas tanto al
-- proyecto 'Vamos México' como al proyecto 'Querétaro Limpio'.
SELECT pv.RazonSocial
FROM Proveedores pv
WHERE pv.RazonSocial IN (
  SELECT pv.RazonSocial
  FROM Proveedores pv, Proyectos py, Entregan e
  WHERE pv.RFC = e.RFC
  AND  py.Numero = e.Numero
  AND py.Denominacion LIKE 'Vamos Mexico%'
)
AND pv.RazonSocial IN (
  SELECT pv.RazonSocial
  FROM Proveedores pv, Proyectos py, Entregan e
  WHERE pv.RFC = e.RFC
  AND  py.Numero = e.Numero
  AND py.Denominacion LIKE 'Queretaro Limpio%'
)

-- Descripción de los materiales que nunca han sido entregados al proyecto 'CIT Yucatán'
SELECT m.Descripcion
FROM Materiales m
WHERE m.Descripcion  NOT IN (
  SELECT m.Descripcion
  FROM Materiales m, Entregan e, Proyectos py
  WHERE m.Clave = e.Clave
  AND py.Numero = e.Numero
  AND py.Denominacion LIKE 'CIT Yucatan%'
  GROUP BY m.Descripcion
)

-- Razón social y promedio de cantidad entregada de los proveedores cuyo promedio de cantidad
-- entregada es mayor al promedio de la cantidad entregada por el proveedor con el RFC 'VAGO780901'.
SELECT pv.RazonSocial, AVG(Cantidad) as 'Promedio de cantidad entregada'
FROM Proveedores pv, Entregan e
WHERE pv.RFC = e.RFC
AND pv.RFC NOT LIKE 'VAGO780901'
GROUP BY pv.RazonSocial

-- RFC, razón social de los proveedores que participaron en el proyecto 'Infonavit Durango' y cuyas
-- cantidades totales entregadas en el 2000 fueron mayores a las cantidades totales entregadas en el 2001.
SET DATEFORMAT DMY
SELECT pr.RFC, pr. RazonSocial
FROM Proveedores pr, Entregan e, Proyectos py
WHERE pr.RFC = e.RFC
AND py.Numero = e.Numero
AND py.Denominacion  LIKE 'Infonavit Durango%'
AND (
  SELECT SUM(e.cantidad)
  FROM Entregan e, Proyectos py, Proveedores pr
  WHERE py.Numero = e.Numero
  AND pr.RFC = e.RFC
  AND py.Denominacion  LIKE 'Infonavit Durango%'
  AND e.Fecha BETWEEN '01/01/2000' and '31/12/2000'

  ) > (
  SELECT SUM(e.cantidad)
  FROM Entregan e, Proyectos py, Proveedores pr
  WHERE py.Numero = e.Numero
  AND pr.RFC = e.RFC
  AND py.Denominacion  LIKE 'Infonavit Durango%'
  AND e.Fecha BETWEEN '01/01/2001' and '31/12/2001'
)
