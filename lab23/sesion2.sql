--Revisa el contenido de la tabla clientes_banca desde la ventana que inicializaste como la segunda sesión.
SELECT * FROM CLIENTES_BANCA

--Inserta la siguiente transacción y ejecútala.
BEGIN TRANSACTION PRUEBA2
INSERT INTO CLIENTES_BANCA VALUES('004','Ricardo Rios Maldonado',19000)
INSERT INTO CLIENTES_BANCA VALUES('005','Pablo Ortiz Arana',15000)
INSERT INTO CLIENTES_BANCA VALUES('006','Luis Manuel Alvarado',18000)

SELECT * FROM CLIENTES_BANCA

--Intenta con la siguiente consulta desde la segunda sesión.
SELECT * FROM CLIENTES_BANCA where NoCuenta='001'

--Por último regresa a la ventana que mantiene activa tu primer sesión, agrega el siguiente comando a la pantalla y ejecútalo.
ROLLBACK TRANSACTION PRUEBA2

--Revisa nuevamente el contenido de la tabla clientes_banca desde la ventana que inicializaste como la segunda sesión.
SELECT * FROM CLIENTES_BANCA
